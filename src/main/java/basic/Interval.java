package basic;


import java.util.Scanner;

/**
 * @author Maksym Sukhovii1
 * @version 1.0
 */

public class Interval {
    private int firstNumber;
    private int secondNumber;

    public Interval(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public Interval() {

    }

    /**
     * All parametrs needed to this method are getting from console
     */
    public Interval setValueFromConsole() {
        Interval intervalFromConsole = new Interval();
        System.out.println("Input please the begin of the interval");
        int beginInterval = isInt();
        System.out.println("Input please the end of the interval");
        int endInterval = isInt();
        setFirstNumber(Math.min(beginInterval, endInterval));
        setSecondNumber(Math.max(beginInterval, endInterval));
        return intervalFromConsole;

    }

    public void printOddFromStartToEndAndEvenReverse() {
        int firstOdd = getFirstNumber();
        int sumOfOddNumbers = 0;
        if (firstOdd % 2 == 0) firstOdd++;
        int oddNumber = firstOdd;
        while (oddNumber <= getSecondNumber()) {
            System.out.print(oddNumber + "; ");
            sumOfOddNumbers += oddNumber;
            oddNumber += 2;
        }
        System.out.println("The sum of odd numbers from the interval is " + sumOfOddNumbers);
        System.out.println();
        int sumOfEvenNumbers = 0;
        int secondNumb = getSecondNumber();
        int lastEven = (secondNumb % 2 == 0) ? secondNumb : --secondNumb;
        int evenNumber = lastEven;
        while (evenNumber >= getFirstNumber()) {
            System.out.print(evenNumber + "; ");
            sumOfEvenNumbers += evenNumber;
            evenNumber -= 2;
        }
        System.out.println("The sum of even numbers from the interval is " + sumOfEvenNumbers);
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    /**
     * This method allow input only integer number
     */
    public static int isInt() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNextInt()) {
            System.out.println("Be polite!!! Please, enter the Integer number!!!");
            sc.next();
        }
        ;
        return sc.nextInt();
    }
}
