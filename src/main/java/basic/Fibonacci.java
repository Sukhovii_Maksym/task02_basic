package basic;

import java.util.Scanner;

/**
 * @author Maksym Sukhovii1
 * @version 1.0
 */

public class Fibonacci {
    private int amount;

    public Fibonacci() {

    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Fibonacci(int amount) {
        this.amount = amount;
    }

    /**
     * This method set amount
     */
    public void set() {
        System.out.println("Enter amount of Fibonacci numbers please");
        int amount = isInt();
        setAmount(amount);
    }

    /**
     * This method builds Fibonacci row
     */
    public int[] getFibonacciRow() {
        int[] fibonacciRow = new int[getAmount()];
        fibonacciRow[0] = 1;
        if (getAmount() > 0) {
            fibonacciRow[1] = 1;
        }
        for (int i = 2; i < amount; i++) {
            fibonacciRow[i] = fibonacciRow[i - 1] + fibonacciRow[i - 2];
        }
        return fibonacciRow;
    }

    /**
     * This method solves task from EPAM Training
     */
    public void printTheBiggestOddAndEven() {
        int lastInRow = getFibonacciRow()[getAmount() - 1];
        int F1;
        int F2;
        if (lastInRow % 2 == 0) {
            F2 = lastInRow;
            F1 = getFibonacciRow()[getAmount() - 2];
        } else {
            F1 = lastInRow;
            F2 = getFibonacciRow()[getAmount() - 2];
        }
        System.out.println("The biggest odd fibonacci " + F1);
        if (F2 > 1) {
            System.out.println("The biggest even fibonacci " + F2);
        } else {
            System.out.println("There isn't any even number");
        }
    }
/**Output to console*/
    public void printPercentOfOddAndEven() {
        double percent;
        int oddNumber = 0;
        int[] fibonacciRow = getFibonacciRow();
        for (int i = 0; i < getAmount(); i++) {
            if (fibonacciRow[i] % 2 == 1) {
                oddNumber++;
            }
        }
        percent = 100 * oddNumber / getAmount();
        System.out.printf("Odd fibonacci %.2f percent. ", percent);
        System.out.printf("Even fibonacci %.2f percent", 100 - percent);
    }

    public static int isInt() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNextInt()) {
            System.out.println("Be polite!!! Please, enter the Integer number!!!");
            sc.next();
        }

        return sc.nextInt();
    }

}
